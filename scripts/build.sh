rm -rf build
mkdir -p build
source venv/bin/activate
pex -v gonoteit/ -r requirements.txt --disable-cache -c gonoteit -o build/gonoteit.pex
deactivate
