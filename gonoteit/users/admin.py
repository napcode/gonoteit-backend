from django.contrib import admin
from django.db import models
from django_json_widget.widgets import JSONEditorWidget

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'is_superuser', 'is_staff')
    formfield_overrides = {
        models.JSONField: {'widget': JSONEditorWidget},
    }
