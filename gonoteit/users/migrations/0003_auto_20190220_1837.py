# Generated by Django 2.0.5 on 2019-02-20 18:37

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20180504_1140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='data',
            field=django.contrib.postgres.fields.jsonb.JSONField(),
        ),
    ]
