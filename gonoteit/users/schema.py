import json
import graphene

from graphene.types.generic import GenericScalar

from gonoteit.decorators import method_permissions, classmethod_permissions
from gonoteit.permissions import IsAuthenticated


def json_resolver(root, info, fields=None):
    if not fields:
        return root.data

    return {
        field: root.data[field] for field in set(root.data.keys()).intersection(fields)
    }


class UserType(graphene.ObjectType):
    id = graphene.Int()
    username = graphene.String()
    data = GenericScalar(fields=graphene.List(graphene.String), resolver=json_resolver)


class Query:
    user = graphene.Field(UserType)

    @method_permissions([IsAuthenticated])
    def resolve_user(self, info, **kwargs):
        user = info.context.user

        return user


class UpdateUser(graphene.Mutation):
    class Arguments:
        data = graphene.String(required=False)
        fields = graphene.List(graphene.String, required=False)

    user = graphene.Field(UserType)
    ok = graphene.Boolean()

    @classmethod
    @classmethod_permissions([IsAuthenticated])
    def mutate(cls, root, info, **kwargs):
        user = info.context.user
        fields = kwargs.pop('fields', [])
        data = json.loads(kwargs.pop('data', {}))

        if fields:
            for field in fields:
                if field in data:
                    user.data[field] = data[field]
        elif data:
            user.data = data

        user.save()

        return UpdateUser(user=user, ok=True)


class Mutation(graphene.ObjectType):
    update_user = UpdateUser.Field()
