from graphql import GraphQLError


class IsAuthenticated:
    @staticmethod
    def check(info, **kwargs):
        if not info.context.user.is_authenticated:
            raise GraphQLError('You\'re not authenticated')


class IsSuperUser:
    @staticmethod
    def check(info, **kwargs):
        if not info.context.user.is_superuser:
            raise GraphQLError('You\'re not super user')
