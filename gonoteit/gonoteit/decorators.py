from functools import wraps


def method_permissions(permissions):
    def permissions_decorator(func):
        @wraps(func)
        def func_wrapper(obj, info, **kwargs):
            for permission in permissions:
                permission.check(info, **kwargs)

            return func(obj, info, **kwargs)

        return func_wrapper

    return permissions_decorator


def classmethod_permissions(permissions):
    def permissions_decorator(func):
        @wraps(func)
        def func_wrapper(cls, root, info, **kwargs):
            for permission in permissions:
                permission.check(info, cls=cls, root=root, **kwargs)

            return func(cls, root, info, **kwargs)

        return func_wrapper

    return permissions_decorator
