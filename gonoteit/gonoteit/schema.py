import graphene
import graphql_jwt

import entities.schema
import users.schema


class JWTMutations(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Query(entities.schema.Query, users.schema.Query, graphene.ObjectType):
    pass


class Mutation(
    JWTMutations, entities.schema.Mutation, users.schema.Mutation, graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
