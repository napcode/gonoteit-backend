import json
import time
import graphene

from datetime import datetime, timezone
from graphene.types.generic import GenericScalar

from gonoteit.decorators import method_permissions, classmethod_permissions
from gonoteit.permissions import IsAuthenticated
from entities.permissions import HasEntityReadAccess, HasEntityWriteAccess

from .models import Entity, DeletedEntity
from . import enums


TypeEnum = graphene.Enum.from_enum(enums.Type)
AccessEnum = graphene.Enum.from_enum(enums.Access)
RecurringNoteGroupEnum = graphene.Enum.from_enum(enums.RecurringNoteGroup)


def json_resolver(root, info, fields=None):
    if not fields:
        return root.data

    return {
        field: root.data[field] for field in set(root.data.keys()).intersection(fields)
    }


def datetime_resolver(root, info):
    if root.date is None:
        return None

    return int(root.date.timestamp())


class EntityType(graphene.ObjectType):
    id = graphene.Int()
    tags = graphene.List(graphene.String)
    type = TypeEnum()
    read_access = AccessEnum()
    write_access = AccessEnum()
    created_at = graphene.Int()
    updated_at = graphene.Int()
    data = GenericScalar(fields=graphene.List(graphene.String), resolver=json_resolver)
    date = graphene.Int(resolver=datetime_resolver)


class Query:
    entity = graphene.Field(
        EntityType,
        id=graphene.Int(),
    )
    all_entities = graphene.List(EntityType)
    calendar_entities = graphene.Field(
        graphene.List(EntityType), date_from=graphene.Int(), date_to=graphene.Int()
    )
    favorite_entities = graphene.List(EntityType)
    timestamp = graphene.Int()

    @method_permissions([HasEntityReadAccess])
    def resolve_entity(self, info, **kwargs):
        return Entity.objects.filter(pk=kwargs.get('id')).first()

    def resolve_all_entities(self, info, **kwargs):
        user = info.context.user

        return Entity.all_entities_for(user).order_by('-_updated_at')

    def resolve_calendar_entities(self, info, **kwargs):
        user = info.context.user
        entities = Entity.all_entities_for(user).filter(date__isnull=False)

        if 'date_from' in kwargs and kwargs['date_from'] is not None:
            date_from = datetime.fromtimestamp(kwargs['date_from'], timezone.utc)
            entities = entities.filter(date__gte=date_from)

        if 'date_to' in kwargs and kwargs['date_to'] is not None:
            date_to = datetime.fromtimestamp(kwargs['date_to'], timezone.utc)
            entities = entities.filter(date__gte=date_to)

        return entities.order_by('date')

    def resolve_favorite_entities(self, info, **kwargs):
        user = info.context.user
        if user.is_authenticated and 'favorite' in user.data:
            return (
                Entity.all_entities_for(user)
                .filter(pk__in=user.data['favorite'])
                .order_by('-_updated_at')
            )

        return None

    def resolve_timestamp(self, info, **kwargs):
        return time.mktime(datetime.now().timetuple())


class CreateEntity(graphene.Mutation):
    class Arguments:
        tags = graphene.List(graphene.String, required=False)
        type = TypeEnum(required=True)
        read_access = AccessEnum(required=False)
        write_access = AccessEnum(required=False)
        data = graphene.String(required=True)
        date = graphene.Int(required=False)

    entity = graphene.Field(EntityType)
    related_entities = graphene.List(EntityType)
    ok = graphene.Boolean()

    @classmethod
    @classmethod_permissions([IsAuthenticated])
    def mutate(cls, root, info, **kwargs):
        kwargs = cls._prepare_data(info, kwargs)

        entity = Entity.objects.create(**kwargs)
        related_entities = []

        if entity.type == enums.Type.RECURRING_NOTE:
            entity.data['parent'] = entity.id
            entity.save()
            related_entities = cls._create_recurring(entity, **kwargs)

        return CreateEntity(entity=entity, related_entities=related_entities, ok=True)

    @classmethod
    def _prepare_data(cls, info, kwargs):
        kwargs['data'] = json.loads(kwargs['data'])
        kwargs['owner'] = info.context.user

        if 'date' in kwargs:
            kwargs['date'] = datetime.fromtimestamp(kwargs['date'], timezone.utc)

        return kwargs

    @classmethod
    def _create_recurring(cls, parent, **kwargs):
        data = kwargs.get('data', {})
        data['parent'] = parent.id
        dates = data.get('dates', [])
        related_entities = []
        for date in dates:
            kwargs['date'] = datetime.fromtimestamp(date, timezone.utc)
            entity = Entity.objects.create(**kwargs)
            related_entities.append(entity)

        return related_entities


class UpdateEntity(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        tags = graphene.List(graphene.String, required=False)
        type = TypeEnum(required=False)
        read_access = AccessEnum(required=False)
        write_access = AccessEnum(required=False)
        data = graphene.String(required=False)
        fields = graphene.List(graphene.String, required=False)
        date = graphene.Int(required=False)
        apply_to = RecurringNoteGroupEnum(required=False)

    entity = graphene.Field(EntityType)
    related_entities = graphene.List(EntityType)
    ok = graphene.Boolean()

    @classmethod
    @classmethod_permissions([HasEntityWriteAccess])
    def mutate(cls, root, info, id, **kwargs):
        if not Entity.objects.filter(pk=id).exists():
            return UpdateEntity(ok=False)

        fields, data, kwargs = cls._prepare_data(info, kwargs)

        entity = Entity.objects.get(pk=id)
        related_entities = []
        if entity.type == enums.Type.RECURRING_NOTE:
            entity, related_entities = cls._update_recurring(
                entity.id, kwargs, fields, data
            )
        else:
            entity = cls._update(id, kwargs, fields, data)

        return UpdateEntity(entity=entity, related_entities=related_entities, ok=True)

    @classmethod
    def _prepare_data(cls, info, kwargs):
        fields = kwargs.pop('fields', [])
        data = json.loads(kwargs.pop('data', '{}'))
        if 'date' in kwargs:
            date = kwargs['date']
            if date is None or date == -1:
                kwargs['date'] = None
            else:
                kwargs['date'] = datetime.fromtimestamp(date, timezone.utc)

        return fields, data, kwargs

    @classmethod
    def _update(cls, id, kwargs, fields, data):
        kwargs.pop('apply_to', None)  # Entity doesn't have such field
        Entity.objects.filter(pk=id).update(**kwargs)
        entity = Entity.objects.get(pk=id)

        if data:
            if fields:
                for field in fields:
                    if field in data:
                        entity.data[field] = data[field]
            else:
                entity.data = data

            entity.save()

        return entity

    @classmethod
    def _update_recurring(cls, id, kwargs, fields, data):
        apply_to = kwargs.get('apply_to', enums.RecurringNoteGroup.THIS)
        date = kwargs.get('date')
        entities = []
        if apply_to == enums.RecurringNoteGroup.THIS:
            entities = [id]
        elif apply_to == enums.RecurringNoteGroup.ALL:
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE, data__parent=id
            ).values_list('id', flat=True)
        elif apply_to == enums.RecurringNoteGroup.THIS_AND_ALL_NEXT:
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE,
                data__parent=id,
                date__gte=date,
            ).values_list('id', flat=True)
        elif apply_to == enums.RecurringNoteGroup.ALL_NEXT:
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE, data__parent=id, date__gt=date
            ).values_list('id', flat=True)
        elif apply_to == enums.RecurringNoteGroup.THIS_AND_ALL_PREVIOUS:
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE,
                data__parent=id,
                date__lte=date,
            ).values_list('id', flat=True)
        elif apply_to == enums.RecurringNoteGroup.ALL_PREVIOUS:
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE, data__parent=id, date__lt=date
            ).values_list('id', flat=True)

        if apply_to != enums.RecurringNoteGroup.THIS:
            # It will be a bad idea to update many recurring notes dates
            kwargs.pop('date')

        related_entities = []
        for entity_id in entities:
            updated_entity = cls._update(entity_id, kwargs, fields, data)
            if entity_id != id:
                related_entities.append(updated_entity)

        return Entity.objects.get(pk=id), related_entities


class DeleteEntity(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        apply_to = RecurringNoteGroupEnum(required=False)

    deleted = graphene.Boolean()
    related_entities = graphene.List(graphene.Int)

    @classmethod
    @classmethod_permissions([IsAuthenticated, HasEntityWriteAccess])
    def mutate(cls, root, info, id, **kwargs):
        if not Entity.objects.filter(pk=id).exists():
            return DeleteEntity(deleted=False)

        entity = Entity.objects.get(pk=id)
        related_entities = []
        if entity.type == enums.Type.RECURRING_NOTE:
            deleted_entities = cls._delete_recurring(entity, **kwargs)
            DeletedEntity.objects.bulk_create(deleted_entities)
            related_entities = [
                e.entity_id for e in deleted_entities if e.entity_id != id
            ]
        else:
            cls._delete(entity)
            DeletedEntity.objects.create(
                entity_id=id, read_access=entity.read_access, owner=entity.owner
            )

        return DeleteEntity(deleted=True, related_entities=related_entities)

    @classmethod
    def _delete(cls, entity):
        entity.delete()

    @classmethod
    def _delete_recurring(cls, entity, **kwargs):
        apply_to = kwargs.get('apply_to', enums.RecurringNoteGroup.THIS)
        parent = entity.data['parent']
        entities = Entity.objects.none()
        deleted_entities = []
        if apply_to == enums.RecurringNoteGroup.ALL:
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE, data__parent=parent
            )
        elif apply_to in (
            enums.RecurringNoteGroup.THIS_AND_ALL_NEXT,
            enums.RecurringNoteGroup.ALL_NEXT,
        ):
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE,
                data__parent=parent,
                date__gt=entity.date,
            )
        elif apply_to in (
            enums.RecurringNoteGroup.THIS_AND_ALL_PREVIOUS,
            enums.RecurringNoteGroup.ALL_PREVIOUS,
        ):
            entities = Entity.objects.filter(
                type=enums.Type.RECURRING_NOTE,
                data__parent=parent,
                date__lt=entity.date,
            )

        deleted_entities = [
            DeletedEntity(entity_id=e.id, read_access=e.read_access, owner=e.owner)
            for e in entities
        ]
        entities.delete()

        if apply_to in (
            enums.RecurringNoteGroup.THIS,
            enums.RecurringNoteGroup.THIS_AND_ALL_PREVIOUS,
            enums.RecurringNoteGroup.THIS_AND_ALL_NEXT,
        ):
            deleted_entities.append(
                DeletedEntity(
                    entity_id=entity.id,
                    read_access=entity.read_access,
                    owner=entity.owner,
                )
            )
            entity.delete()

        return deleted_entities


class Changelog(graphene.Mutation):
    class Arguments:
        timestamp = graphene.Int(required=True)

    created = graphene.List(EntityType)
    updated = graphene.List(EntityType)
    deleted = graphene.List(graphene.Int)
    timestamp = graphene.Int()

    @classmethod
    @classmethod_permissions([IsAuthenticated])
    def mutate(cls, root, info, timestamp, **kwargs):
        user = info.context.user
        dt = datetime.fromtimestamp(timestamp, timezone.utc)

        changelog_timestamp = time.mktime(datetime.now().timetuple())
        created = Entity.all_entities_for(user).filter(_created_at__gt=dt)
        updated = Entity.all_entities_for(user).filter(_updated_at__gt=dt)
        deleted = (
            DeletedEntity.all_entities_for(user)
            .filter(timestamp__gt=dt)
            .values_list('entity_id', flat=True)
        )

        return Changelog(
            created=created,
            updated=updated,
            deleted=deleted,
            timestamp=changelog_timestamp,
        )


class Mutation(graphene.ObjectType):
    create_entity = CreateEntity.Field()
    update_entity = UpdateEntity.Field()
    delete_entity = DeleteEntity.Field()
    changelog = Changelog.Field()
