from django.db.models.signals import pre_delete
from django.dispatch import receiver

from .models import Entity, DeletedEntity


@receiver(pre_delete, sender=Entity, dispatch_uid='create_deleted_entity')
def create_deleted_entity(sender, instance, created, **kwargs):
    DeletedEntity.objects.create(
        entity_id=instance.id, read_access=instance.read_access, owner=instance.owner
    )
