from graphql import GraphQLError

from entities.models import Entity
from entities.enums import Access


class HasEntityReadAccess:
    @staticmethod
    def check(info, **kwargs):
        user = info.context.user
        entity = Entity.objects.filter(pk=kwargs.get('id')).first()

        if not entity:
            raise GraphQLError('You don\'t have read access')

        if entity.read_access == Access.PUBLIC:
            return True

        if entity.read_access == Access.PRIVATE and entity.owner == user:
            return True

        if entity.read_access == Access.INTERNAL and user.is_authenticated:
            return True

        raise GraphQLError('You don\'t have read access')


class HasEntityWriteAccess:
    @staticmethod
    def check(info, **kwargs):
        user = info.context.user
        entity = Entity.objects.filter(pk=kwargs.get('id')).first()

        if not entity:
            raise GraphQLError('You don\'t have read access')

        if entity.write_access == Access.PUBLIC:
            return True

        if entity.write_access == Access.PRIVATE and entity.owner == user:
            return True

        if entity.write_access == Access.INTERNAL and user.is_authenticated:
            return True

        raise GraphQLError('You don\'t have read access')
