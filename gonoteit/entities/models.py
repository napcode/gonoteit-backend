import time
import datetime

from django.db import models
from django.db.models import Q

from django.contrib.postgres.fields import ArrayField

from users.models import User

from . import enums


class Entity(models.Model):
    tags = ArrayField(models.CharField(max_length=20), null=True, blank=True)
    type = models.IntegerField(
        default=enums.Type.NONE, choices=[(o.value, o.name) for o in enums.Type]
    )
    read_access = models.IntegerField(
        default=enums.Access.INTERNAL, choices=[(o.value, o.name) for o in enums.Access]
    )
    write_access = models.IntegerField(
        default=enums.Access.INTERNAL, choices=[(o.value, o.name) for o in enums.Access]
    )
    _created_at = models.DateTimeField(auto_now_add=True)
    _updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='owned_entities'
    )
    data = models.JSONField()
    date = models.DateTimeField(null=True, blank=True)

    @property
    def created_at(self):
        return time.mktime(self._created_at.timetuple())

    @created_at.setter
    def created_at(self, timestamp):
        self._created_at = datetime.datetime.fromtimestamp(
            timestamp, datetime.timezone.utc
        )

    @property
    def updated_at(self):
        return time.mktime(self._updated_at.timetuple())

    @updated_at.setter
    def updated_at(self, timestamp):
        self._updated_at = datetime.datetime.fromtimestamp(
            timestamp, datetime.timezone.utc
        )

    @property
    def type_display_name(self):
        return enums.Type(self.type).name

    @property
    def updated_at_formatted(self):
        return self._updated_at.strftime('%Y-%m-%d %H:%M')

    @staticmethod
    def all_entities_for(user=User):
        if user.is_authenticated:
            return Entity.objects.filter(
                Q(read_access=enums.Access.PUBLIC)
                | Q(read_access=enums.Access.INTERNAL)
                | Q(owner=user)
            )

        return Entity.objects.filter(read_access=enums.Access.PUBLIC)

    class Meta:
        verbose_name_plural = "entities"


class DeletedEntity(models.Model):
    entity_id = models.IntegerField()
    read_access = models.IntegerField()
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='owned_deleted_entities'
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def all_entities_for(user=User):
        if user.is_authenticated:
            return DeletedEntity.objects.filter(
                Q(read_access=enums.Access.PUBLIC)
                | Q(read_access=enums.Access.INTERNAL)
                | Q(owner=user)
            )

        return DeletedEntity.objects.filter(read_access=enums.Access.PUBLIC)

    class Meta:
        verbose_name_plural = "deleted entities"
