from django.contrib import admin
from django.db import models
from django_json_widget.widgets import JSONEditorWidget

from .models import Entity, DeletedEntity


class VisibilityListFilter(admin.SimpleListFilter):
    title = 'visibility'
    parameter_name = 'visibility'

    def lookups(self, request, model_admin):
        return (('my', 'My'),)

    def queryset(self, request, queryset):
        if self.value() == 'my':
            return Entity.all_entities_for(request.user)


@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    list_display = ('id', 'type_display_name', 'owner', 'updated_at_formatted')
    list_filter = (VisibilityListFilter,)
    ordering = ['-_updated_at']
    formfield_overrides = {
        models.JSONField: {'widget': JSONEditorWidget},
    }


@admin.register(DeletedEntity)
class DeletedEntityAdmin(admin.ModelAdmin):
    list_display = ('id', 'entity_id', 'owner', 'timestamp')
