import io
import base64

from PIL import Image

from django.http import HttpResponse, Http404
from django.views.generic import View

from .models import Entity


class ImageView(View):
    def get(self, request, note_id):
        entity = Entity.objects.filter(pk=note_id).first()

        if entity is None or 'imageBase64' not in entity.data:
            raise Http404

        image = base64.b64decode(entity.data['imageBase64'])

        if 'thumbnail' in request.GET:
            image = Image.open(io.BytesIO(image))
            image.thumbnail((75, 75), Image.ANTIALIAS)
            response = HttpResponse(content_type="image/jpeg")
            image.save(response, "JPEG")

            return response

        return HttpResponse(image, content_type="image/jpeg")
