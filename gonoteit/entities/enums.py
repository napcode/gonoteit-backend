from enum import IntEnum


class Type(IntEnum):
    NONE = 0
    NOTE = 1
    LIST = 2
    CALENDAR_NOTE = 3
    RECURRING_NOTE = 4


class Access(IntEnum):
    PUBLIC = 0
    PRIVATE = 1
    INTERNAL = 2


class RecurringNoteGroup(IntEnum):
    THIS = 0
    ALL = 1
    THIS_AND_ALL_NEXT = 2
    ALL_NEXT = 3
    THIS_AND_ALL_PREVIOUS = 4
    ALL_PREVIOUS = 5
